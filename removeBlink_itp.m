% -----------------------------------------------------------
% removeBlink_itp ver.01
% 2016.11.18
% 瞳孔面積データ内の瞬目による欠損値を, 線形補間により補間する.
% interp1()  :: 1次線形補間
%
% SINTAX :: removeBlink_itp()
% param   : N/A
% return  : obj result
% -----------------------------------------------------------

function result = removeBlink_itp()
	% //////////////////////////////////////////
	% ファイルの入力
	% //////////////////////////////////////////
	
	% カレントディレクトリにある'*.csv'という名前のファイル('*'はワイルドカードと言って, 全部という意味です)一覧を取得
    dataNameList = dir('*.csv');
    
    % ファイル個数分だけループ(カレントディレクトリ内のすべてのファイルについて自動で実行するため)
    for fileNum = 1 : length(dataNameList)
      
		fileName = char(dataNameList(fileNum).name); % [fileNum]番目のファイルについて'pupilLeft01.csv'という文字列を読み込み
		dataName = fileName(1:length(fileName)-4); % [fileNum]th '.csv'の4文字を抜いて'pupilLeft'という文字列作成
		pathName = strcat(dataName, '/', fileName); % 'pupilLeft01/pupilLeft01.csv'という文字列作成
		if exist(dataName, 'dir') ~= 7 % dataName() % 'pupilLeft01'という名前のディレクトリがなかったら
			mkdir(dataName); % 'pupilLeft01'という名前のディレクトリ作成
		end
		movefile(fileName, pathName); % rawデータを'pupilLeft01'という名前のディレクトリに移動
		
		
		% ファイルの中身読み込み
		pupilMatrix = csvread(pathName); % csvread関数でpupilMatrixという行列変数に格納
		pupilMatrixBefore = csvread(pathName); % 確認用に一応2つ読み込んでおく(なくてもいい)
%		pupilMatrix = csvread('pupilLeft01.csv'); % for debug
		
		
		% //////////////////////////////////////////
		% 補間処理
		% //////////////////////////////////////////

		% Validity(=pupilMatrix(:,2)つまりpupilMatrixの全行2列目)をフラグに, エラー時のインデックス(=行番号)取得
		% 保険として, Validity以外に, pupil値(=pupilMatrix(:,1)つまり全行1列目)が-1のときもエラーと認識させています
		errorIndex = find( ( pupilMatrix(:,2)==4 | pupilMatrix(:,2)==3 ) |  pupilMatrix(:,1)==-1) ;
		
		% 瞬目ごとの行番号を格納する行列 とりま50x1で初期化
		errorRange = zeros(50, 1);
		
		% エラー行数分だけループを回して調べたら終了するための終了地点
		endPoint = length(errorIndex);
		
		i = 1; % エラー行数インクリメント用
		k = 1; % 瞬目範囲インクリメント用
		
		% エラーが起こってた行数分だけループを回して, 逐一調べていく
		while i < endPoint
			
			errorRange(k) = errorIndex(i); % 瞬目範囲の最初から, 逐一エラー行番号を入力していく
			
			% 次のエラー行番号が現在のエラー行番号+1でなかったら(次の瞬目に飛んでいたら)
			if errorIndex(i+1) ~= errorIndex(i)+1
			
				% 現在errorRangeに入っているのが1回の瞬目の行番号たちなので,
				% 補間処理を開始する
				
				% 0のマージンを削除
				errorRange = errorRange( errorRange(:)~=0 ); % errorRangeに, errorRangeのうち (全errorRangeの中で0じゃないやつ) 代入
				
				% 補間の開始と終了地点を設定
				startId = errorRange(1) - 1; % Validityが4or3の1つ前の点から
				goalId = errorRange(length(errorRange)) + 1; % Validityが4or3の1つ後ろの点まで
				
				% 補間用変数たち設定
				x1 = [ startId : (goalId-startId) : goalId]; % 補間するx軸の開始から終了 startとgoalの列ベクトル
															 % 例) 3から10までなら(3 : 7 : 10) つまり 3から10まで公差7の等差数列なので, [3 10]
				v1 = [ pupilMatrix(startId) : (pupilMatrix(goalId)-pupilMatrix(startId)) : pupilMatrix(goalId)]; % 補間するy軸の開始から終了
															 % 例) 4.2から3.7までなら(4.2 : -0.5 : 3.7) つまり 4.2から3.7まで公差-0.5の等差数列なので, [4.2 3.7]
				x2 = [ startId : 1 : goalId ]; % 補間するx軸の範囲と間隔
															 % 例) 3から10までなら(3 : 1 : 10) つまり 3から10まで公差1の等差数列なので, [3 4 5 ... 8 9 10]
				v2 = interp1 ( x1, v1, x2 ); % 補間
				%plot(x1, v1, 'o', x2, v2, '*'); % for debug
				
				% 補間したベクトルv2でpupilMatrixを更新
				pupilMatrix(startId:goalId, 1) = v2'; % interp1は結果を行ベクトルで返すっぽいので転置(')させて列ベクトルに
				pupilMatrix(startId:goalId, 2) = 1; % Validityを1で更新して3or4っていうフラグに引っかからないよう更新
				
				
				% 瞬目範囲用変数リセット
				errorRange = zeros(50, 1); % 0でリセット
				k = 0; % インクリメントも0でリセット
				
		    end % 補間
			
			i = i + 1; % エラーインデックスをインクリメント
			k = k + 1; % 瞬目をインクリメント
			
		end % while
		
		
		% 確認用のグラフ出力
		hold off % 前回のグラフをクリア
		fig = plot(pupilMatrixBefore(:,1)); % リサンプリング前のグラフ描画
		hold on % 複数のグラフを描画する用のコマンド
		fig = plot(pupilMatrix(:,1)); % リサンプリング後のグラフ描画
		fig = legend('before', 'remove Blink by interpolation'); % 凡例
		
		
		
		
		% FFTとかいれるならここ
		
		
		
		
		% //////////////////////////////////////////
		% ファイル出力
		% //////////////////////////////////////////
		% 信号(pupilMatrix)を出力
		pupilSignalFileName = strcat(dataName, '/', dataName, '_itp.csv'); % 'pupilLeft01/pupilLeft01_itp.csv'という文字列作成
		csvwrite(pupilSignalFileName, pupilMatrix(:,1)); % csvwrite()を使ってpupilMatrixの全行1列目を書き込み
		fileID = fopen(pupilSignalFileName, 'w'); % C系のI/0のファイル操作の常套手段 : 1.fopenして
		fprintf(fileID, '%f\n', pupilMatrix(:,1)); % C系のI/0のファイル操作の常套手段2 : 2.fprintして
		fclose(fileID); % C系のI/0のファイル操作の常套手段 : 3.fcloseする
       	
		% グラフ(fig)を出力
		pupilFigFileName = strcat(dataName, '/', dataName, '_itp.png'); % 'pupilLeft01/pupilLeft01_itp.png'という文字列作成
		saveas(fig, pupilFigFileName); % 図の保存にはsaveas()という関数を使うらしい
		
		
		
	end % ファイル個数分だけ繰り返し
	
	
	
end % END OF removeBlink()