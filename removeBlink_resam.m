% -----------------------------------------------------------
% removeBlink_resam ver.01
% 2016.11.19
% 瞳孔面積データ内の瞬目による欠損値を, 線形補間により補間する.
% resample() :: 等間隔リサンプリングによる内挿
% 
% SINTAX :: removeBlink_resam()
% param   : N/A
% return  : obj result
% -----------------------------------------------------------

function result = removeBlink_resam()
	% //////////////////////////////////////////
	% ファイルの入力
	% //////////////////////////////////////////
	
	% カレントディレクトリにある'*.csv'という名前のファイル('*'はワイルドカードと言って, 全部という意味です)一覧を取得
	dataNameList = dir('*.csv');
	
	% ファイル個数分だけループ(カレントディレクトリ内のすべてのファイルについて自動で実行するため)
	for fileNum = 1 : length(dataNameList)
		
		fileName = char(dataNameList(fileNum).name); % [fileNum]番目のファイルについて'pupilLeft01.csv'という文字列を読み込み
		dataName = fileName(1:length(fileName)-4); % [fileNum]th '.csv'の4文字を抜いて'pupilLeft'という文字列作成
		pathName = strcat(dataName, '/', fileName); % 'pupilLeft01/pupilLeft01.csv'という文字列作成
		if exist(dataName, 'dir') ~= 7 % dataName() % 'pupilLeft01'という名前のディレクトリがなかったら
		    mkdir(dataName); % 'pupilLeft01'という名前のディレクトリ作成
		end
		movefile(fileName, pathName); % rawデータを'pupilLeft01'という名前のディレクトリに移動
		
		
		% ファイルの中身読み込み
		pupilMatrix = csvread(pathName); % csvread関数でpupilMatrixという行列変数に格納
		pupilMatrixBefore = csvread(pathName); % 確認用に一応2つ読み込んでおく(なくてもいい)
%		pupilMatrix = csvread('pupilLeft01.csv'); % for debug
		
		
		% //////////////////////////////////////////
		% 補間処理 (リサンプリング)
		% //////////////////////////////////////////
		
		% Validity(=pupilMatrix(:,2)つまりpupilMatrixの全行2列目)をフラグに, エラー時のインデックス(=行番号)取得
		% 保険として, Validity以外に, pupil値(=pupilMatrix(:,1)つまり全行1列目)が-1のときもエラーと認識させています
		errorIndex = find( ( pupilMatrix(:,2)==4 | pupilMatrix(:,2)==3 ) |  pupilMatrix(:,1)==-1) ;
		
		% エラーのインデックスをNaN(つまり欠損値)で置き換え(resampe()の仕様)
		pupilMatrix(errorIndex,1) = NaN;
		
		% データ点の数の列ベクトル用意(DSPだと時間ベクトルと呼びます 多分)
		dataPoint = ( 1 : length(pupilMatrix(:,1)) );
		
		% pupilMatrix全行1列目を, データ点において, リサンプリングする
		% このresample()が, NaNについては勝手に線形補間してくれる仕様になってることを利用しています
		pupilResample = resample(pupilMatrix(:,1), dataPoint);
		
		
		% リサンプリング確認用のグラフ描画
		hold off % 前回のグラフをクリア
		fig_resam = plot(pupilMatrixBefore(:,1)); % リサンプリング前のグラフ描画
		hold on % 複数のグラフを描画する用のコマンド
		fig_resam = plot(pupilResample(:,1)); % リサンプリング後のグラフ描画
		fig_resam = legend('before', 'remove Blink by resample'); % 凡例
		
		% リサンプリング後グラフ(fig_resam)をファイル出力
		pupilResamFigFileName = strcat(dataName, '/', dataName, '_resam.png'); % 'pupilLeft01/pupilLeft01_resam.png'という文字列作成
		saveas(fig_resam, pupilResamFigFileName); % 図の保存にはsaveas()という関数を使うらしい
		
		
		
		
		
		% FFTとかの処理入れるならこのあたりだと思われます
		% //////////////////////////////////////////
		% 正規化
		% //////////////////////////////////////////
		pupilZscore = zscore( pupilResample ); %正規化して平均値を0, SDを1にする
		
		% 正規化確認用のグラフ描画
		hold off % 前回のグラフをクリア
		fig_zscore = plot(pupilZscore); % リサンプリング後のグラフ描画
		fig_zscore = legend('zscore'); % 凡例
		
		% 正規化後グラフ(fig_zscore)をファイル出力
		pupilZscoreFigFileName = strcat(dataName, '/', dataName, '_zscore.png'); % 'pupilLeft01/pupilLeft01_zscore.png'という文字列作成
		saveas(fig_zscore, pupilZscoreFigFileName); % 図の保存にはsaveas()という関数を使うらしい
		
		
		
		
		
		% //////////////////////////////////////////
		% リサンプリング後/正規化後信号のファイル出力
		% //////////////////////////////////////////
		% リサンプリング信号(pupilResample)を出力
		pupilResamFileName = strcat(dataName, '/', dataName, '_resam.csv'); % 'pupilLeft01/pupilLeft01_resam.csv'という文字列作成
		csvwrite(pupilResamFileName, pupilResample); % csvwrite()を使ってpupilResampleの全行1列目を書き込み
		
		fileID_resam = fopen(pupilResamFileName, 'w'); % C系のI/0のファイル操作の常套手段 : 1.fopenして
		fprintf(fileID_resam, '%f\n', pupilResample); % C系のI/0のファイル操作の常套手段2 : 2.fprintして
		fclose(fileID_resam); % C系のI/0のファイル操作の常套手段 : 3.fcloseする
		
		
		
		% 正規化信号(pupilZscore)を出力
		pupilZscoreFileName = strcat(dataName, '/', dataName, '_zscore.csv'); % 'pupilLeft01/pupilLeft01_zscore.csv'という文字列作成
		csvwrite(pupilZscoreFileName, pupilZscore); % csvwrite()を使ってpupilZscoreの全行1列目を書き込み
		
		fileID_zscore = fopen(pupilZscoreFileName, 'w'); % C系のI/0のファイル操作の常套手段 : 1.fopenして
		fprintf(fileID_zscore, '%f\n', pupilZscore); % C系のI/0のファイル操作の常套手段2 : 2.fprintして
		fclose(fileID_zscore); % C系のI/0のファイル操作の常套手段 : 3.fcloseする
		
		
		
		
		
		
	end % ファイル個数分だけ繰り返し
	
	
	
end % END OF removeBlink()